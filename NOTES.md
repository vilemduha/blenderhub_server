# BlenderHub dev notes

## Qs:

* asset model:
    * je potřeba uchovávat různé verze assetu?
    * co všechno se může změnit, abychom to považovali za změnu verze a ne nový asset?

* asset styles/engines/shaders/(materials)
    * jsou společné pro všehny asset types, nebo má každý z nich jinou sadu?

## API notes
* new asset version - based on existing asset
    * should the asset attributes be optional and copied from base asset?  

## Models

* validation:
    * validation for asset and assetVersion

* ratings:
    * rating for asset and assetVersion

### Asset
* asset fields - tags vs. engines, styles
* assets files
    * files should be downloadable only with a proper token provided


### Asset versioning
    * only change of the .bland file means new version
    * files (.blend, thumbnails) are connected to version
    * file de-duplication - if the file is the same between versions, use the old one

## API

* API versioning
* Schemas
    * drf-yasg https://github.com/axnsan12/drf-yasg/
    * Django Rest Swagger (not maintained)
        * Comprehensive approach https://github.com/m-haziq/django-rest-swagger-docs
    * DRF OpenAPI (not maintained) https://github.com/limdauto/drf_openapi
* Swagger API definition
    * reponse objects, status codes

### Auth

* token
* JWT

### Assets endpoint

* /assets/<uuid>/

### File upload API
* it's not possible to upload file to a assets version with some file uploaded
* checks: file type, max size, upload size (is upload complete?), antivirus
* file access permissions (valid token required)
    * XSendfile https://www.nginx.com/resources/wiki/start/topics/examples/xsendfile/
    * https://kovyrin.net/2006/11/01/nginx-x-accel-redirect-php-rails/
* !!! https://philsturgeon.uk/api/2016/01/04/http-rest-api-file-uploads/
* https://docs.djangoproject.com/en/2.0/topics/http/file-uploads/

* purge stale/unfinished uploads

### Search endpoint

* TODO

## Notifications

* PostgreSQL PUBSUB - NOTIFY/LISTEN
    * http://initd.org/psycopg/docs/advanced.html#asynchronous-notifications
    * https://blog.andyet.com/2015/04/06/postgres-pubsub-with-json/
    * https://dba.stackexchange.com/questions/170976/postgres-listen-notify-as-message-queue
        * https://blog.2ndquadrant.com/what-is-select-skip-locked-for-in-postgresql-9-5/

* celery
* django-channels (v2)
    * https://www.aeracode.org/2017/7/11/towards-channels-20/


## Search

* TODO

## Deployment

* uwsgi
* supervisor
* nginx

