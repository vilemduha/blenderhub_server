#!/usr/bin/env bash

git pull
pip install -r requirements_deploy.txt
pip install -U .
manage.py migrate
manage.py collectstatic --noinput
uwsgi --reload /home/blenderhub/run/blenderhub.pid
