import os

import django_heroku

from blenderhub.settings.project import *  # noqa
from blenderhub.settings.project import LOGGING, RAVEN_CONFIG, logging


DEFAULT_HOST = 'blenderkit-devel.herokuapp.com'

STATIC_URL = '/s/'

MEDIA_URL = '/m/'

LOGGING['root'] = {
    'level': 'INFO',
    'handlers': ['sentry_severe', 'console'],
}
LOGGING['handlers']['console']['level'] = 'INFO'

if os.environ.get('SENTRY_DSN'):
    RAVEN_CONFIG.update({
        'environment': os.environ.get('ENVIRONMENT')
    })

    # set up handlers for defined loggers
    for logger_name in LOGGING['loggers']:
        # do not override explicitly disabled loggers
        if 'null' not in LOGGING['loggers'][logger_name]['handlers']:
            LOGGING['loggers'][logger_name]['handlers'].append('sentry_severe')

EMAIL_HOST_USER = os.environ['SENDGRID_USERNAME']
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = os.environ['SENDGRID_PASSWORD']

logging.config.dictConfig(LOGGING)

django_heroku.settings(locals())
