import os

from .project import *  # noqa
from .project import LOGGING, RAVEN_CONFIG, logging


ALLOWED_HOSTS = ['dev.blenderkit.com', 'bh.softic.cz']
DEFAULT_HOST = 'dev.blenderkit.com'

STATIC_URL = '/s/'
STATIC_ROOT = '/home/blenderhub/static/'

MEDIA_URL = '/m/'
MEDIA_ROOT = '/home/blenderhub/media/'

LOGGING['root'] = {
    'level': 'INFO',
    'handlers': ['sentry_severe', 'console'],
}
LOGGING['handlers']['console']['level'] = 'INFO'

if os.environ.get('SENTRY_DSN'):
    RAVEN_CONFIG.update({
        'environment': os.environ.get('ENVIRONMENT')
    })

    # set up handlers for defined loggers
    for logger_name in LOGGING['loggers']:
        # do not override explicitly disabled loggers
        if 'null' not in LOGGING['loggers'][logger_name]['handlers']:
            LOGGING['loggers'][logger_name]['handlers'].append('sentry_severe')

logging.config.dictConfig(LOGGING)
