import logging.config
import os
import re

import dj_database_url
import raven
from psycopg2.extensions import ISOLATION_LEVEL_REPEATABLE_READ


ADMINS = (
    ('Petr Dlouhý', 'petr.dlouhy@email.cz'),
    ('Vilém Duha', 'vilem.duha@gmail.com '),
)
DEFAULT_FROM_EMAIL = 'Blenderhub <vilem.duha@gmail.com >'
MANAGERS = ADMINS


BASE_DIR = os.getenv('BASE_DIR', os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

SECRET_KEY = os.environ.get('SECRET_KEY')

DEBUG = False

ALLOWED_HOSTS = []
DEFAULT_HOST = None

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_extensions',
    'drf_yasg',
    'raven.contrib.django.raven_compat',
    'rest_framework',
    'rest_framework.authtoken',
    'author',
    'polymorphic',
    'rest_polymorphic',
    'adminsortable2',
    'storages',
    'registration',
    'easy_thumbnails',
    'related_admin',
    'nested_admin',

    'blenderhub.apps.accounts',
    'blenderhub.apps.api',
    'blenderhub.apps.assets',
    'blenderhub.apps.core',
    'blenderhub.apps.evaluation',
    'blenderhub.apps.frontend',
    'blenderhub.apps.parameters',
    'blenderhub.apps.search',
    'blenderhub.apps.uploads',
    'blenderhub.apps.usage_statistics',
]

MIDDLEWARE = [
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'author.middlewares.AuthorDefaultBackendMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'blenderhub.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django_settings_export.settings_export',
            ],
        },
    },
]

WSGI_APPLICATION = 'blenderhub.wsgi.application'

# Database
db_from_env = dj_database_url.config(conn_max_age=600)
DATABASES = {
    'default': db_from_env
}

DATABASES['default'].update({
    'ATOMIC_REQUESTS': True,
    'OPTIONS': {
        # 'isolation_level': ISOLATION_LEVEL_SERIALIZABLE,
        'isolation_level': ISOLATION_LEVEL_REPEATABLE_READ,
    },
})

if os.environ.get('DATABASE_PASSWORD'):
    DATABASES['default']['PASSWORD'] = os.environ.get('DATABASE_PASSWORD')

# Auth
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

USE_I18N = True
USE_L10N = True

TIME_ZONE = 'Europe/Prague'
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'staticfiles'))
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, os.pardir, 'media'))
FILE_UPLOAD_PERMISSIONS = 0o644

REDIS_URL = os.environ.get('REDIS_URL', 'redis://127.0.0.1:6379')

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_URL + "/0",
        "KEY_PREFIX": 'bh_default',
        "TIMEOUT": None,
        "OPTIONS": {
            "PARSER_CLASS": "redis.connection.HiredisParser",
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "COMPRESSOR": "django_redis.compressors.zlib.ZlibCompressor",
        }
    },
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = 'default'

# Logging
LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        }
    },
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(server_time)s] %(message)s',
        },
        'verbose': {
            # 'format': '%(levelname)s %(asctime)s %(pathname)s:%(lineno)d '
            #           '%(module)s %(process)d %(thread)d %(message)s',
            'format': '%(levelname)s %(module)s %(process)d %(thread)d %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },

        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            # 'filters': ['require_debug_true'],
            'formatter': 'verbose',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },

        'sentry_severe': {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            # 'formatter': 'verbose',
        },
        'sentry': {
            'level': 'INFO',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            # 'formatter': 'verbose',
        },
        'sentry_debug': {
            'level': 'DEBUG',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            # 'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },

        # 'django.db': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },

        'celery': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },

        'celery.redirected': {
            'handlers': ['null'],
            'level': 'ERROR',
            'propagate': False,
        },

        # 'requests': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        #     'propagate': True,
        # },

        'blenderhub': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}
logging.config.dictConfig(LOGGING)

try:
    RELEASE = raven.fetch_git_sha(os.path.abspath(os.path.join(BASE_DIR, os.pardir))),
except raven.exceptions.InvalidGitRepository:
    RELEASE = os.getenv('HEROKU_SLUG_COMMIT')

RAVEN_CONFIG = {
    # 'dsn': '',  # SENTRY_DSN env var used
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': RELEASE,
}

# REST API
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.BrowsableAPIRenderer',
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'rest_framework.parsers.FileUploadParser',
    ),
    'TEST_REQUEST_RENDERER_CLASSES': (
        'rest_framework.renderers.MultiPartRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.TemplateHTMLRenderer'
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'blenderhub.apps.api.authentication.BearerTokenAuthentication',
        'blenderhub.apps.api.authentication.SuperAdminSessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.IsAuthenticated',
        # 'rest_framework.permissions.IsAdminUser',
    ],

    'EXCEPTION_HANDLER': 'blenderhub.apps.api.exceptions.exception_handler',

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,

    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    # 'EXCEPTION_HANDLER': 'rocketreport.apps.api.exception_handler.exception_handler'
}

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        # 'basic': {
        #     'type': 'basic'
        # }
        "Bearer API key": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        },
    },
    'USE_SESSION_AUTH': True,
    'JSON_EDITOR': True,
}

REDOC_SETTINGS = {
    'LAZY_RENDERING': True,
}

SETTINGS_EXPORT_VARIABLE_NAME = 'cfg'
SETTINGS_EXPORT = [
    'DEBUG',
    'DEFAULT_HOST',
    'PROJECT_NAME',
]

# BK: app settings
PROJECT_NAME = 'BlenderKit'
ES_INDEX_ASSETS = 'bk.assets'

AUTH_USER_MODEL = "accounts.UserProfile"

# ELASTICSEARCH
es_url = os.environ.get('BONSAI_URL', os.environ.get('ES_URL', None))
if es_url:
    es_auth = re.search('https\:\/\/(.*)\@', es_url).group(1).split(':')
    es_host = es_url.replace('https://%s:%s@' % (es_auth[0], es_auth[1]), '')

    # Connect to cluster over SSL using auth for best security:
    ES_HEADER = [{
        'host': es_host,
        'port': 443,
        'use_ssl': True,
        'http_auth': (es_auth[0], es_auth[1])
    }]
    ES_ENABLED = True
else:
    ES_ENABLED = False

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_S3_HOST = os.environ.get('AWS_S3_HOST', 's3.amazonaws.com')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME', 'blenderkit')
if AWS_ACCESS_KEY_ID:
    THUMBNAIL_DEFAULT_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = '/'

THUMBNAIL_ALIASES = {
    '': {
        'admin': {'size': (96, 96), 'smart': True},
        'api': {'size': (96, 96), 'smart': True},
    },
}
