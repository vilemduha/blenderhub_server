from .project import *  # noqa
from .project import LOGGING, logging


DEBUG = True

DEFAULT_HOST = 'localhost:8000'
SECRET_KEY = '@n-kxut2&8$ut1vly)z=)d&$oh0=rgel-u140b^rd3t@x=00s&'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

AUTH_PASSWORD_VALIDATORS = []

LOGGING['loggers']['django.db'] = {
    'handlers': ['console'],
    'level': 'ERROR',
    'propagate': False,
}
logging.config.dictConfig(LOGGING)

ES_INDEX_ASSETS = 'bk.debug.assets'

ALLOWED_HOSTS = [
    'lvh.me',
    'localhost',
]
