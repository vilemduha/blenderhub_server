from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from djchoices import ChoiceItem, DjangoChoices

from blenderhub.apps.assets.models import Asset


class Scene(TimeStampedModel):
    user_profile = models.ForeignKey(
        "accounts.UserProfile",
        verbose_name=_("user profile"),
        on_delete=models.CASCADE,
    )
    scene_uuid = models.UUIDField(
        _("scene id"),
        editable=True,
        null=False,
        blank=False,
    )

    def __str__(self):
        return "Scene %i by %s" % (self.scene_uuid, self.user_profile)


class UsageReport(TimeStampedModel):
    class ReportType(DjangoChoices):
        download = ChoiceItem('download', _("At download"))
        save = ChoiceItem('save', _("At save event"))
        scene_change = ChoiceItem('scene_change', _("At scene change"))
    scene = models.ForeignKey(
        Scene,
        verbose_name=_("scene"),
        on_delete=models.CASCADE,
    )
    report_type = models.CharField(
        _("report type"),
        choices=ReportType.choices,
        max_length=50,
        null=False,
        blank=False,
        default='download',
    )


class AssetUsage(TimeStampedModel):
    asset = models.ForeignKey(
        "assets.Asset",
        verbose_name=_("asset"),
        on_delete=models.PROTECT,
    )
    usage_report = models.ForeignKey(
        UsageReport,
        verbose_name=_("usage report"),
        on_delete=models.CASCADE,
    )
    usage_count = models.PositiveIntegerField(
        'usage count',
        default=1,
        null=False,
        blank=False,
    )

    def __str__(self):
        return "Usage of asset %s" % (self.asset,)


class Proximity(TimeStampedModel):
    asset_usage = models.ForeignKey(
        AssetUsage,
        verbose_name=_("asset usage"),
        on_delete=models.CASCADE,
    )
    asset_placed_near = models.ForeignKey(
        Asset,
        verbose_name=_("near asset usage"),
        on_delete=models.PROTECT,
        related_name='near_backwards',
    )
    distance = models.FloatField(
        _("distance"),
        validators=[MinValueValidator(0.0)],
        null=True,
        blank=True,
    )
