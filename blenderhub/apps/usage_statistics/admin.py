import nested_admin
from django.contrib import admin
from related_admin import RelatedFieldAdmin

from . import models


@admin.register(models.Scene)
class SceneAdmin(RelatedFieldAdmin):
    list_display = (
        'scene_uuid',
        'user_profile__email',
        'created',
    )


class ProximityInline(nested_admin.NestedTabularInline):
    model = models.Proximity
    fk_name = 'asset_usage'
    extra = 0
    readonly_fields = (
        'created',
    )


class AssetUsageInline(nested_admin.NestedTabularInline):
    model = models.AssetUsage
    extra = 0
    inlines = (ProximityInline,)
    readonly_fields = (
        'created',
    )


@admin.register(models.UsageReport)
class UsageReportAdmin(RelatedFieldAdmin, nested_admin.NestedModelAdmin):
    list_display = (
        'id',
        'scene__scene_uuid',
        'scene__user_profile__email',
        'created',
    )
    inlines = (AssetUsageInline,)
    readonly_fields = (
        'created',
    )
