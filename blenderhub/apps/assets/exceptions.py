from django.core.exceptions import ValidationError


class InvalidAssetVersionError(ValidationError):
    def __init__(self, message=None, code=None, params=None):
        message = message or 'Asset has a newer version.'
        code = code or 'invalid_asset_version'
        super().__init__(message, code, params)
