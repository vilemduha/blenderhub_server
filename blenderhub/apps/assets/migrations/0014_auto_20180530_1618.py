# Generated by Django 2.0.5 on 2018-05-30 14:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assets', '0013_auto_20180523_1239'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='assetfile',
            unique_together={('asset', 'file_type')},
        ),
    ]
