# Generated by Django 2.0.2 on 2018-03-19 14:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assets', '0009_assettype_value'),
    ]

    operations = [
        migrations.RenameField(
            model_name='asset',
            old_name='asset_type_fk',
            new_name='asset_type',
        ),
    ]
