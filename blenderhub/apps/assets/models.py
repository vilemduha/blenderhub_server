import copy
import uuid

from author.decorators import with_author
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import NON_FIELD_ERRORS, ValidationError
from django.db import models, transaction
from django.db.models import Avg, Max, Window
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from djchoices import ChoiceItem, DjangoChoices
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer

from . import exceptions
from ..evaluation.models import Rating


User = get_user_model()


class AssetQuerySet(models.QuerySet):
    def filter_newest(self):
        return self \
            .order_by('-asset_uuid', '-version_number') \
            .distinct('asset_uuid')

    def filter_versions(self, asset):
        return self.filter(asset_uuid=asset.asset_uuid)

    def is_newest(self, asset):
        newest_asset = self.filter_versions(asset=asset).order_by('-version_number')[0]
        return newest_asset.version_number == asset.version_number

    def get_max_version_number(self, asset):
        return self.values_list('version_number', flat=True) \
            .filter(asset_uuid=asset.asset_uuid)\
            .order_by('-version_number') \
            .first()

    def annotate_max_version_number(self):
        return self.annotate(max_version_number=Window(
            expression=Max('version_number'),
            partition_by=[models.F('asset_uuid')],
        ))

    def related_api(self):
        return self.select_related('author')

    def filter_api_list(self, author):
        # noinspection PyUnresolvedReferences
        return self.filter_newest().filter(author=author).related_api()


# TODO: design: consider business logic in forms?
class AssetManager(models.Manager.from_queryset(AssetQuerySet)):
    @transaction.atomic
    def create_version(self, asset=None, **kwargs):
        if not asset:
            # create first version
            kwargs['version_number'] = 1
            new_asset = Asset(**kwargs)

        else:
            # update and create new version
            new_asset = copy.copy(asset)

            for kw, val in kwargs.items():
                setattr(new_asset, kw, val)

            new_asset.id = None
            new_asset.version_uuid = None

            max_version_number = Asset.objects.get_max_version_number(asset)
            new_asset.version_number = max_version_number + 1

        new_asset.full_clean()
        new_asset.save()

        return new_asset

    # TODO: validation: only selected attributes can be updated
    # TODO: decision: only verified version can be updated?
    @transaction.atomic
    def update_version(self, asset, **kwargs):
        if not Asset.objects.is_newest(asset=asset):
            raise ValidationError({NON_FIELD_ERRORS: exceptions.InvalidAssetVersionError()})

        updated = False

        for kw, val in kwargs.items():
            if getattr(asset, kw) != val:
                updated = True
                setattr(asset, kw, val)

        if updated:
            asset.full_clean()
            asset.save()

        return asset


class AssetType(models.Model):
    name = models.CharField(
        _("name"),
        max_length=255,
    )
    value = models.SlugField(
        _("identifier"),
    )

    def __str__(self):
        return self.name


@with_author
class Asset(TimeStampedModel):
    class AssetCondition(DjangoChoices):
        new = ChoiceItem('new', _("new"))
        used = ChoiceItem('used', _("used"))
        old = ChoiceItem('old', _("old"))
        desolate = ChoiceItem('desolate', _("desolate"))

    class ProductionLevel(DjangoChoices):
        template = ChoiceItem('template', _("template"))
        finished = ChoiceItem('finished', _("finished"))

    class License(DjangoChoices):
        royalty_free = ChoiceItem('royalty_free', _("royalty free"))
        cc_zero = ChoiceItem('cc_zero', _("creative commons zero"))

    asset_uuid = models.UUIDField(_("asset id"), editable=False)
    version_uuid = models.UUIDField(_("version id"), editable=False)

    version_number = models.PositiveIntegerField(_("version number"), default=1)

    # metadata
    # TODO: store in single json field? common fields?
    name = models.CharField(_("name"), max_length=200)
    description = models.TextField(_("description"), blank=True)

    asset_type = models.ForeignKey(AssetType, _("asset type"))

    # TODO: array of char choices or many2many?
    # asset_styles = ArrayField(models.CharField(max_length=100), verbose_name=_("asset styles"))
    # asset_engines = ArrayField(models.CharField(max_length=100), verbose_name=_("asset engines"))

    tags = ArrayField(models.CharField(_("tag"), max_length=20), verbose_name=_("tags"),
                      default=list, blank=True, null=True)
    # TODO: design year: ancient/stone age?
    # design_year = models.CharField(_("design year"), max_length=20, blank=True, null=True)
    license = models.CharField(_("license"), max_length=100, choices=License.choices)
    adult = models.BooleanField(_("adult content"), default=False)

    # source
    source_app_name = models.CharField(_("source app name"), max_length=200)
    source_app_version = models.CharField(_("source app version"), max_length=200)
    addon_version = models.CharField(_("addon version"), max_length=200)

    # verification status
    # status:
    # being created (editing attributes, uploading files)
    # ready to be verified
    # being verified
    # "is on hold" - creator can fix
    # is rejected
    # is accepted
    is_verified = models.BooleanField(_("is verified"), default=False)

    # TODO: validation

    objects = AssetManager()

    class Meta:
        # FIXME: ordering defined here is not compatible with AssetQuerySet.filter_newest
        # ordering = ('-pk',)
        verbose_name = _("asset")
        verbose_name_plural = _("assets")
        unique_together = (
            ('asset_uuid', 'version_uuid'),
            ('asset_uuid', 'version_number'),
        )

    def __str__(self):
        return '%s / v%s / %s' % (
            self.version_uuid,
            self.version_number,
            self.name,
        )

    def save(self, **kwargs):
        if not self.asset_uuid:
            self.asset_uuid = uuid.uuid4()

        if not self.version_uuid:
            self.version_uuid = uuid.uuid4()

        super().save(**kwargs)

    def get_thumbnail_first(self):
        thumbnail_first = self.files.filter(
            file_type=AssetFile.FileType.thumbnail,
        ).first()
        return thumbnail_first

    def thumbnail_small_fld(self):
        file_fld = self.get_thumbnail_first()
        if file_fld:
            return file_fld.thumbnail_small_fld()
        else:
            return "-"
    thumbnail_small_fld.short_description = _("thumbnail")

    def average_rating(self, rating_type):
        rating_set = Asset.objects.filter(pk=self.pk)
        rating_set = rating_set.filter(rating__rating_type=rating_type)
        rating_set = rating_set.annotate(avg_score=Avg('rating__score'))
        if rating_set.exists():
            return rating_set[0].avg_score


def average_rating_generator(rating_type, rating_description):
    def average_rating(self):
        return self.average_rating(rating_type)
    average_rating.short_description = rating_description
    return average_rating


for rating_type, rating_description in Rating.RatingType.choices:
    setattr(Asset, 'average_' + rating_type, average_rating_generator(rating_type, rating_description))


# class AssetAttributeBase(TimeStampedModel):
#     title = models.CharField(_("title"), max_length=100)
#     description = models.TextField(_("description"), blank=True)
#
#     available_to = models.ForeignKey(User, help_text=_("available to user"),
#                                      on_delete=models.PROTECT, null=True, blank=True)
#     is_enabled = models.BooleanField(_("is enabled"), default=True)
#
#     class Meta:
#         abstract = True


# class AssetStyle(AssetAttributeBase):
#     pass


# TODO: file_type blend => .blend extension
def asset_file_upload_to(instance: 'AssetFile', filename):
    path = 'assets/{version_uuid}/files/{file_type}_{file_uuid}.{ext}'

    try:
        f_name, ext = filename.rsplit('.', 1)
    except ValueError:
        f_name, ext = filename, 'ext'  # noqa

    file_uuid = instance.uuid

    return path.format(
        version_uuid=instance.asset.version_uuid.hex,
        file_type=instance.file_type,
        file_uuid=file_uuid,
        ext=ext,
    )


class AssetFileQuerySet(models.QuerySet):
    pass


class AssetFileManager(models.Manager.from_queryset(AssetFileQuerySet)):
    def create_asset_file(self, **kwargs):
        asset_file = AssetFile(**kwargs)

        asset_file.full_clean()
        asset_file.save()

        return asset_file


# TODO: validation: single blend file, multiple thumbnails
class AssetFile(TimeStampedModel):
    class FileType(DjangoChoices):
        blend = ChoiceItem('blend', _("blend file"))
        thumbnail = ChoiceItem('thumbnail', _("thumbnail"))

    uuid = models.UUIDField(_("asset file id"), editable=False)

    file_type = models.CharField(max_length=50, choices=FileType.choices)
    file_upload = models.FileField(upload_to=asset_file_upload_to, blank=True, null=True)

    asset = models.ForeignKey(Asset, on_delete=models.CASCADE, related_name='files')

    objects = AssetFileManager()

    class Meta:
        verbose_name = _("asset file")
        verbose_name_plural = _("asset files")
        unique_together = ('asset', 'file_type')

    def __str__(self):
        return 'File: %s of %s' % (self.file_type, self.asset.version_uuid,)

    def save(self, **kwargs):
        if not self.uuid:
            self.uuid = uuid.uuid4()

        super().save(**kwargs)

    def thumbnail_fld(self):
        thumbnail = None
        if self.file_type == self.FileType.thumbnail:
            thumbnail = self.file_upload

        if not thumbnail:
            return '-'

        return format_html("<img src='{}' alt='thumbnail'>", thumbnail.url)
    thumbnail_fld.short_description = _("thumbnail")

    def thumbnail_small_fld(self):
        thumbnail = None
        if self.file_type == self.FileType.thumbnail:
            thumbnail = self.file_upload

        if not thumbnail:
            return '-'

        try:
            return format_html(
                "<a target='_blank' href='{url}'><img width='96' src='{thumb_url}' alt='thumbnail'></a>",
                thumb_url=get_thumbnailer(thumbnail)['admin'].url,
                url=thumbnail.url,
            )
        except InvalidImageFormatError:
            return "Image error"
    thumbnail_small_fld.short_description = _("thumbnail")


@receiver(pre_delete, sender=AssetFile, dispatch_uid='assetfile_delete_signal')
def delete_files(sender, instance, using, **kwargs):
    instance.file_upload.delete()
