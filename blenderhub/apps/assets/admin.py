from django import forms
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
from polymorphic.admin import PolymorphicInlineSupportMixin

from blenderhub.apps.core.admin import DefaultFilterMixin
from blenderhub.apps.core.utils import sizeof_fmt
from blenderhub.apps.evaluation import models as evaluation_models
from blenderhub.apps.parameters.admin import ParameterInline

from . import models


class AssetAdminForm(forms.ModelForm):
    class Meta:
        model = models.Asset
        fields = '__all__'


class OnlyLastVersionsListFilter(SimpleListFilter):
    title = _('newest versions')
    parameter_name = 'newest_versions'

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter_newest()

    def lookups(self, request, model_admin):
        return (
            ('0', _('All versions')),
            ('1', _('Newest versions only')),
        )

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == str(lookup),
                'query_string': changelist.get_query_string({self.parameter_name: lookup}, []),
                'display': title,
            }


@admin.register(models.AssetType)
class AssetTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')
    prepopulated_fields = {"value": ("name",)}


class AssetFileInline(admin.TabularInline):
    model = models.AssetFile
    extra = 0
    readonly_fields = ('uuid', 'thumbnail_small_fld')


class AssetAdmin(PolymorphicInlineSupportMixin, DefaultFilterMixin, admin.ModelAdmin):
    form = AssetAdminForm

    save_as = True
    list_display = [
        'thumbnail_small_fld',
        'id',
        'is_verified',
        'version_uuid',
        'asset_uuid',
        'version_number',
        'name',
        'author',
        'created',
    ]
    evaluation_models.append_average_rating_fields(list_display)
    list_display_links = ('version_uuid',)
    search_fields = (
        'name',
        'asset_uuid',
        'version_uuid',
        'author__username',
    )
    list_filter = (
        OnlyLastVersionsListFilter,
        'asset_type',
        'author',
        'adult',
        'license',
    )
    # FIXME: clashes with AssetQuerySet.filter_newest
    # date_hierarchy = 'created'

    list_select_related = ('author',)
    autocomplete_fields = ('author',)
    readonly_fields = [
        'author',
        'updated_by',
        'asset_uuid',
        'version_uuid',
    ]
    evaluation_models.append_average_rating_fields(readonly_fields)
    inlines = (
        AssetFileInline,
        ParameterInline,
    )

    def get_queryset(self, request):
        _pr = ('files',)
        return super().get_queryset(request).prefetch_related(*_pr)

    def get_default_filters(self, request):
        return {
            'newest_versions': '1',
        }


class AssetFileAdminForm(forms.ModelForm):
    class Meta:
        model = models.AssetFile
        fields = '__all__'


class AssetFileAdmin(admin.ModelAdmin):
    form = AssetFileAdminForm

    list_display = ('thumbnail_small_fld', '__str__', 'asset', 'file_type', 'uuid', 'file_info_fld')
    list_display_links = ('__str__',)
    search_fields = ('file_type', 'asset__asset_uuid', 'asset__version_uuid')
    date_hierarchy = 'created'

    list_select_related = ('asset',)
    autocomplete_fields = ('asset',)
    readonly_fields = ('uuid', 'thumbnail_fld')

    def file_info_fld(self, obj):
        # return "{space}, {width}x{height} px".format(
        #     space=sizeof_fmt(obj.file_upload.size),
        #     width=obj.file_upload.width,
        #     height=obj.file_upload.height,
        # )
        if obj.file_upload:
            return "{space}".format(
                space=sizeof_fmt(obj.file_upload.size),
            )

    file_info_fld.short_description = _("file info")


admin.site.register(models.Asset, AssetAdmin)
admin.site.register(models.AssetFile, AssetFileAdmin)
