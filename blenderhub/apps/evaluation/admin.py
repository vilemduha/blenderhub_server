from django.contrib import admin

from . import models


@admin.register(models.Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = (
        'rating_type',
        'score',
        'asset',
        'user_profile',
    )


@admin.register(models.Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = (
        'asset',
        'user_profile',
    )
