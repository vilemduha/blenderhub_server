# from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from djchoices import ChoiceItem, DjangoChoices


class Rating(TimeStampedModel):
    class Meta:
        verbose_name = _("asset rating")
        verbose_name_plural = _("asset ratings")
        unique_together = ('asset', 'user_profile', 'rating_type')

    class UserEditableRatingType(DjangoChoices):
        quality = ChoiceItem('quality', _("Quality"))
        virtual_price = ChoiceItem('virtual_price', _("Virtual price"))
        complexity = ChoiceItem('complexity', _("Complexity"))

    class RatingType(UserEditableRatingType):
        virtual_price_admin = ChoiceItem('virtual_price_admin', _("Virtual price (internal)"))
        complexity_admin = ChoiceItem('complexity_admin', _("Complexity (internal)"))

    rating_type = models.CharField(
        _("rating type"),
        choices=RatingType.choices,
        max_length=50,
        null=False,
        blank=False,
    )
    score = models.FloatField(
        _("score"),
        null=False,
        blank=False,
        # Temporarily disable to allow rating by virtual price
        # validators=[
        #     MinValueValidator(0),
        #     MaxValueValidator(10),
        # ],
    )
    asset = models.ForeignKey(
        "assets.Asset",
        verbose_name=_("asset"),
        on_delete=models.CASCADE,
    )
    user_profile = models.ForeignKey(
        "accounts.UserProfile",
        verbose_name=_("user profile"),
        on_delete=models.CASCADE,
    )


def append_average_rating_fields(field_list):
    for rating_type in Rating.RatingType.labels.keys():
        field_list.append('average_' + rating_type)


class Review(TimeStampedModel):
    class Meta:
        verbose_name = _("asset review")
        verbose_name_plural = _("asset reviews")
        unique_together = ('asset', 'user_profile')

    review_text = models.TextField(
        _("review text compliments"),
        null=False,
        blank=False,
        default="",
    )
    review_text_problems = models.TextField(
        _("review text problems"),
        null=False,
        blank=False,
        default="",
    )
    asset = models.ForeignKey(
        "assets.Asset",
        verbose_name=_("asset"),
        on_delete=models.CASCADE,
    )
    user_profile = models.ForeignKey(
        "accounts.UserProfile",
        verbose_name=_("user profile"),
        on_delete=models.CASCADE,
    )
