from django.apps import AppConfig
from django.conf import settings
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _


class CoreAppConfig(AppConfig):
    name = 'blenderhub.apps.core'

    def ready(self):
        super().ready()

        admin.site.site_header = _("{project_name} administration").format(project_name=settings.PROJECT_NAME)
        admin.site.site_title = admin.site.site_header
