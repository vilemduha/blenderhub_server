from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from . import models


class UploadAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'asset', 'file_type', 'user', 'created')
    search_fields = ('uuid', 'metadata', 'asset__version_uuid', 'asset__asset_uuid')
    list_filter = ('user', 'file_type')
    date_hierarchy = 'created'

    list_select_related = ('user', 'asset',)
    autocomplete_fields = ('asset',)
    readonly_fields = (
        'uuid',
        'get_upload_post_url',
        'thumbnail_fld',
    )

    def thumbnail_fld(self, obj):
        thumbnail = None
        if obj.file_type == models.AssetFile.FileType.thumbnail:
            thumbnail = obj.file

        if not thumbnail:
            return '-'

        return format_html("<img src='{}' alt='thumbnail'>", thumbnail.url)

    thumbnail_fld.short_description = _("thumbnail")


admin.site.register(models.Upload, UploadAdmin)
