from django.apps import AppConfig


class UploadsAppConfig(AppConfig):
    name = 'blenderhub.apps.uploads'
