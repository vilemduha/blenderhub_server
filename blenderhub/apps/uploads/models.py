import uuid

from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from rest_framework.reverse import reverse

from blenderhub.apps.assets.models import AssetFile, asset_file_upload_to


User = get_user_model()


class UploadQuerySet(models.QuerySet):
    def related_api(self):
        return self.select_related('user', 'asset')

    def filter_api_list(self, user):
        # noinspection PyUnresolvedReferences
        return self.filter(user=user).related_api()


class UploadManager(models.Manager.from_queryset(UploadQuerySet)):
    def create_upload(self, **kwargs):
        upload = Upload(**kwargs)

        upload.full_clean()
        upload.save()

        return upload


class Upload(TimeStampedModel):
    uuid = models.UUIDField(_("upload id"), editable=False)

    user = models.ForeignKey(User, on_delete=models.PROTECT)

    asset = models.ForeignKey('assets.Asset', verbose_name=_("asset"), on_delete=models.CASCADE)
    file_type = models.CharField(max_length=50, choices=AssetFile.FileType.choices)
    file = models.FileField(upload_to=asset_file_upload_to, blank=True, null=True)

    metadata = JSONField(_("metadata"), default=dict, blank=True)

    objects = UploadManager()

    class Meta:
        verbose_name = _("file upload")
        verbose_name_plural = _("file uploads")

    def __str__(self):
        return 'File upload: %s' % (self.uuid,)

    def get_upload_post_url(self):
        if self.uuid:
            return reverse('v1:uploads-upload-file', kwargs={'upload_id': self.uuid})

    def save(self, **kwargs):
        if not self.uuid:
            self.uuid = uuid.uuid4()

        if self.file:
            AssetFile.objects.filter(
                asset=self.asset,
                file_type=self.file_type,
            ).delete()
            AssetFile.objects.create(
                asset=self.asset,
                file_type=self.file_type,
                file_upload=self.file,
            )

        super().save(**kwargs)
