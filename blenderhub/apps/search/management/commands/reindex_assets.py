from django.core.management import BaseCommand

from blenderhub.apps.search import indexers


class Command(BaseCommand):
    help = 'Refreshes asset search index from scratch'

    def handle(self, *args, **options):
        self.stdout.write('Re-creating asset index')

        indexer = indexers.AssetIndexer()
        indexer.delete_index()
        indexer.create_index()
        result = indexer.reindex()

        self.stdout.write(self.style.SUCCESS("Asset index re-created."))
        self.stdout.write("Result: %s" % (result,))
