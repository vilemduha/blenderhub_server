import logging
import re

from django.conf import settings
from django.db.models import Case, IntegerField, Value, When
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import MatchAll, MultiMatch, Q

from blenderhub.apps.assets.models import Asset


logger = logging.getLogger(__name__)


def to_list(val):
    if isinstance(val, list):
        return val

    return [val]


QUERY_BASE_RE = re.compile(r'((?:[^ ])+)', re.IGNORECASE)


def parse_query(url_query):
    query = dict()

    texts = []
    terms = {}

    m_base = QUERY_BASE_RE.findall(url_query)

    for m in m_base:
        if ':' in m:
            term, val = m.split(':')
            vals = list(filter(lambda v: v, val.split(',')))

            terms[term] = vals

        else:
            texts.append(m)

    if texts:
        query['text'] = ' '.join(texts)

    query.update(terms)

    return query


class Searcher:
    def __init__(self, es=None):
        self.es = es or Elasticsearch(settings.ES_HEADER)

    @property
    def index_name(self):
        raise NotImplementedError

    @property
    def object_type(self):
        raise NotImplementedError

    @property
    def object_model(self):
        raise NotImplementedError

    def get_search_query(self, **kwargs):
        return Search(using=self.es, index=self.index_name)

    def execute(self, **kwargs):
        return self.get_search_query(**kwargs).execute()

    def find_objects(self, **kwargs):
        raise NotImplementedError


class AssetSearcher(Searcher):
    @property
    def index_name(self):
        return settings.ES_INDEX_ASSETS

    @property
    def object_type(self):
        return 'asset'

    @property
    def object_model(self):
        return Asset

    # noinspection PyShadowingBuiltins
    def get_search_query(
            self,
            text=None,
            asset_type=None,
            license=None,
            adult=None,
            tags=None,
            **kwargs
    ):

        query = MatchAll()
        filters = MatchAll()

        if text is not None:
            q_text = MultiMatch(query=text, fields=['name', 'tags', 'description'])
            query &= q_text

        if asset_type is not None:
            f_asset_type = Q('terms', asset_type=to_list(asset_type))
            filters &= f_asset_type

        if license is not None:
            f_license = Q('terms', license=to_list(license))
            filters &= f_license

        # TODO: validate boolean: True, False, 'true', 'false'
        if adult is not None:
            f_adult = Q('terms', adult=to_list(adult))
            filters &= f_adult

        if tags is not None:
            f_tags = Q('terms', tags=to_list(tags))
            filters &= f_tags

        return super().get_search_query().query(query).filter(filters)[0:100]

    def find_objects(self, **kwargs):
        if not kwargs:
            return self.object_model.objects.none()

        results = self.execute(**kwargs)

        if not results:
            return self.object_model.objects.none()

        object_ids = [h.asset_id for h in results]
        cases = [When(version_uuid=version_uuid, then=Value(i)) for i, version_uuid in enumerate(object_ids)]

        return self.object_model.objects \
            .filter(version_uuid__in=object_ids) \
            .annotate(sort=Case(*cases, output_field=IntegerField())) \
            .order_by('sort')
