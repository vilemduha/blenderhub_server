from django.apps import AppConfig


class SearchAppConfig(AppConfig):
    name = 'blenderhub.apps.search'

    def ready(self):
        from . import signals  # noqa
