from django.conf import settings
from elasticsearch import Elasticsearch, helpers

from blenderhub.apps.assets.models import Asset


class Indexer:
    def __init__(self, es=None):
        self.es = es or Elasticsearch(settings.ES_HEADER)

    @property
    def index_name(self):
        raise NotImplementedError

    @property
    def object_type(self):
        raise NotImplementedError

    def get_index_settings(self):
        return {}

    def get_index_mappings(self):
        return {}

    def delete_index(self):
        self.es.indices.delete(index=self.index_name, ignore_unavailable=True)

    def create_index(self):
        self.es.indices.create(
            index=self.index_name,
            body={
                'settings': self.get_index_settings(),
                'mappings': self.get_index_mappings(),
            }
        )

    def get_id(self, obj):
        raise NotImplementedError

    def get_source(self, obj):
        raise NotImplementedError

    def get_objects_iter(self):
        raise NotImplementedError

    def get_actions_reindex_iter(self, objects_iter):
        for obj in objects_iter:
            yield {
                '_op_type': 'index',
                '_index': self.index_name,
                '_type': self.object_type,
                '_id': self.get_id(obj),
                '_source': self.get_source(obj),
            }

    def get_actions_delete_iter(self, objects_iter):
        for obj in objects_iter:
            yield {
                '_op_type': 'delete',
                '_index': self.index_name,
                '_type': self.object_type,
                '_id': self.get_id(obj),
            }

    def reindex(self):
        return self.reindex_objects(self.get_objects_iter())

    def reindex_objects(self, objects):
        return helpers.bulk(
            self.es,
            self.get_actions_reindex_iter(objects_iter=objects),
        )

    def delete_objects(self, objects):
        return helpers.bulk(
            self.es,
            self.get_actions_delete_iter(objects_iter=objects),
        )


class AssetIndexer(Indexer):
    @property
    def index_name(self):
        return settings.ES_INDEX_ASSETS

    @property
    def object_type(self):
        return 'asset'

    def get_index_settings(self):
        return {
            'index': {
                'number_of_shards': 3,
                'number_of_replicas': 0
            }
        }

    def get_index_mappings(self):
        return {
            self.object_type: {
                'properties': {
                    'asset_id': {'type': 'keyword'},
                    'asset_base_id': {'type': 'keyword'},
                    'version_number': {'type': 'integer'},

                    # author - name, id or complex/nested
                    # 'author': { 'type': 'text' }

                    'name': {'type': 'text'},
                    'description': {'type': 'text'},

                    'asset_type': {'type': 'keyword'},
                    'license': {'type': 'keyword'},

                    'adult': {'type': 'boolean'},

                    'tags': {'type': 'keyword'},

                    'created': {
                        'type': 'date',
                        'format': 'strict_date_optional_time||epoch_millis'
                    },
                    'modified': {
                        'type': 'date',
                        'format': 'strict_date_optional_time||epoch_millis'
                    },
                }
            }
        }

    def get_id(self, obj):
        return obj.asset_uuid

    def get_source(self, obj):
        return {
            'asset_id': obj.version_uuid,
            'asset_base_id': obj.asset_uuid,
            'version_number': obj.version_number,

            'name': obj.name,
            'description': obj.description,
            'asset_type': obj.asset_type.value,
            'license': obj.license,

            'adult': obj.adult,

            'tags': obj.tags,

            'created': obj.created,
            'modified': obj.modified,
        }

    def get_objects_iter(self):
        return Asset.objects.filter_newest()
