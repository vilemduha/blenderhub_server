from django.conf import settings
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from blenderhub.apps.assets.models import Asset
from blenderhub.apps.search import indexers


@receiver(post_save, sender=Asset, dispatch_uid='asset_post_save_signal')
def reingex_asset(sender, instance, using, **kwargs):
    if settings.ES_ENABLED:
        indexer = indexers.AssetIndexer()
        indexer.reindex_objects([instance])


@receiver(pre_delete, sender=Asset, dispatch_uid='asset_post_save_signal')
def delete_asset(sender, instance, using, **kwargs):
    if settings.ES_ENABLED:
        indexer = indexers.AssetIndexer()
        indexer.delete_objects([instance])
