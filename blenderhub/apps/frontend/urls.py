from django.urls import path

from .views import homepage, test


urlpatterns = [
    path('', homepage, name='homepage'),
    path('test/<slug:number>/', test, name='test'),
]
