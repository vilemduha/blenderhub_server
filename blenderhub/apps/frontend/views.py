from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render


def homepage(request):
    context = {
        'project_name': settings.PROJECT_NAME,
        'authors': [
            'roman',
            'vilda',
            'zdenek',
        ],
        'user': request.user,
    }

    return render(request, 'frontend/homepage.html', context)


def test(request, number):
    return HttpResponse('test ' + str(number))
