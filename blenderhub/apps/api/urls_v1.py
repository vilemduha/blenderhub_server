from django.conf.urls import url
from django.urls import path

from rest_framework_extensions.routers import NestedRouterMixin

from . import routers
from .views import (
    SCHEMA_VIEW, accounts, assets, enums, evaluation, search, uploads,
    usage_statistics
)


class ExtendedApiRouter(NestedRouterMixin, routers.ApiRouter):
    pass


router = ExtendedApiRouter()
router.register(
    r'assets', assets.AssetViewSet, base_name='assets',
).register(
    r'rating', evaluation.RatingViewSet, base_name='asset_id', parents_query_lookups=['asset__version_uuid'],
)
router.register(r'uploads', uploads.UploadViewSet, base_name='uploads')
router.register(r'usage_report', usage_statistics.UsageReportViewSet, base_name='usage_reports')

app_name = 'api'

urlpatterns = [
    url(
        r'assets/(?P<asset__version_uuid>[^/.]+)/review',
        evaluation.ReviewViewSet.as_view({'get': 'retrieve', 'put': 'update'}),
        name='reviews-detail',
    ),
    path('uploads/<uuid:upload_id>/upload-file/', uploads.UploadFileView.as_view(), name='uploads-upload-file'),

    # path('search/<str:query>/', search.SearchView.as_view(), name='search'),
    path('search/', search.SearchView.as_view(), name='search'),

    url(r'^enums/parameter-types/$', enums.ParameterTypeEnum.as_view(), name='enum_parameter_types'),
    url(r'^enums/asset-types/$', enums.AssetTypeEnum.as_view(), name='enum_asset_types'),
    url(r'^enums/asset-conditions/$', enums.AssetConditionEnum.as_view(), name='enum_asset_conditions'),
    url(r'^enums/production-levels/$', enums.ProductionLevelEnum.as_view(), name='enum_production_levels'),
    url(r'^enums/licenses/$', enums.LicenseEnum.as_view(), name='enum_licenses'),
    url(r'^me/$', accounts.MeView.as_view(), name='me'),

    url(r'^swagger(?P<format>.json|.yaml)$', SCHEMA_VIEW.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^docs/$', SCHEMA_VIEW.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # url(r'^api/v1/redoc/$', SCHEMA_VIEW.with_ui('redoc', cache_timeout=None), name='schema-redoc'),
]

urlpatterns += router.urls
