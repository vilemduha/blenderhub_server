import logging

from django.core.exceptions import ValidationError as ModelValidationError
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.files import get_thumbnailer
from rest_framework import serializers
from rest_framework.fields import UUIDField
from rest_framework.relations import HyperlinkedIdentityField, SlugRelatedField
from rest_framework.serializers import (
    HyperlinkedModelSerializer, ModelSerializer
)

from blenderhub.apps.api.exceptions import handle_django_validation_error
from blenderhub.apps.api.serializers import accounts as account_serializers
from blenderhub.apps.api.serializers import parameters as parameter_serializers
from blenderhub.apps.assets.models import Asset, AssetFile, AssetType
from blenderhub.apps.evaluation import models as evaluation_models
from blenderhub.apps.parameters.models import BaseParameter


logger = logging.getLogger(__name__)


class AssetFileSerializer(ModelSerializer):
    file_path = serializers.FileField(source='file_upload', read_only=True)
    file_thumbnail = serializers.SerializerMethodField()

    class Meta:
        model = AssetFile
        fields = (
            'file_type',
            'file_path',
            'file_thumbnail',
        )

    def get_file_thumbnail(self, obj):
        if obj.file_type == obj.FileType.thumbnail:
            return get_thumbnailer(obj.file_upload)['api'].url


class AssetSerializer(HyperlinkedModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='assets-detail',
        lookup_field='version_uuid',
        lookup_url_kwarg='asset_id',
    )
    id = UUIDField(source='version_uuid', read_only=True,
                   help_text=_('Unique id used for referencing an asset. '
                               'It\'s unique for each asset version.'))
    asset_base_id = UUIDField(source='asset_uuid', required=False,
                              help_text=_('Base asset id is common for all versions of given asset.'))
    author = account_serializers.UserSerializer()
    asset_type = SlugRelatedField(
        slug_field='value',
        queryset=AssetType.objects.all(),
    )
    files = AssetFileSerializer(many=True, read_only=True)
    parameters = parameter_serializers.ParameterSerializer(source="baseparameter_set", many=True)

    class Meta:
        model = Asset
        fields = [
            'url',
            'id',

            'asset_base_id',
            # 'version_uuid',
            'version_number',

            'author',

            'name',
            'description',

            'asset_type',
            'adult',
            'tags',

            'license',
            'parameters',

            'source_app_name',
            'source_app_version',
            'addon_version',

            'created',
            'is_verified',
            'files',
        ]
        evaluation_models.append_average_rating_fields(fields)

        read_only_fields = [
            'url',
            # 'version_uuid',
            'version_number',
            'is_verified',
            'average_complexity',
            'average_quality',
            'average_virtual_price',
        ]
        evaluation_models.append_average_rating_fields(read_only_fields)

    def parse_parameters(self, parameters_data, asset):
        parameters_data = parameters_data
        for parameter_data in parameters_data:
            parameter_data['asset'] = asset
            parameter = parameter_serializers.ParameterSerializer.create(
                parameter_serializers.ParameterSerializer(),
                validated_data=parameter_data,
            )
            parameter.save()

    def validate(self, attrs):
        self.parameters_data = attrs.pop('baseparameter_set')
        return super().validate(attrs)


class AssetRefSerializer(AssetSerializer):
    class Meta:
        model = Asset
        fields = (
            'id',
            'name',
            'version_number',
        )
        read_only_fields = (
            'id',
            'name',
            'version_number',
        )


class AssetCreateSerializer(AssetSerializer):
    asset_base_id = UUIDField(source='asset_uuid', required=False, read_only=False)
    author = account_serializers.UserRefSerializer()
    version_number = UUIDField(required=False, read_only=True)

    class Meta:
        model = Asset
        fields = (
            'url',

            'id',
            'asset_base_id',
            'version_number',

            'name',
            'description',

            'asset_type',
            'adult',
            'tags',

            'license',
            'parameters',

            'source_app_name',
            'source_app_version',
            'addon_version',
        )
        read_only_fields = (
            'url',
        )

    def validate(self, attrs):
        attrs = super().validate(attrs)

        attrs['author'] = self.context['request'].user

        if attrs.get('tags') is None:
            attrs['tags'] = []

        instance = Asset(**attrs)

        try:
            # unique_together (asset_uuid, version_number) should not be checked
            instance.full_clean(exclude=['version_number'])

        except ModelValidationError:
            logger.debug('ValidationError', exc_info=True)
            raise

        asset_uuid = attrs.get('asset_base_id')
        if asset_uuid:
            try:
                asset_base = Asset.objects.filter_api_list(author=attrs['author']).get(asset_uuid=asset_uuid)

            except Asset.DoesNotExist:
                raise serializers.ValidationError(
                    {'asset_base_id': "Invalid asset to base a version on."},
                    code='invalid',
                )

            attrs['asset_base'] = asset_base

        return attrs

    def create(self, validated_data):
        asset_base = validated_data.pop('asset_base', None)
        with handle_django_validation_error():
            asset = Asset.objects.create_version(asset=asset_base, **validated_data)
            self.parse_parameters(self.parameters_data, asset)
            return asset


class AssetUpdateSerializer(AssetSerializer):
    class Meta:
        model = Asset
        fields = (
            'name',
            'description',

            'adult',
            'tags',

            'license',

            'parameters',
            'source_app_name',
            'source_app_version',
            'addon_version',
        )

    def validate(self, attrs):
        return super().validate(attrs)

    def update(self, instance, validated_data):
        with handle_django_validation_error():
            asset = Asset.objects.update_version(asset=instance, **validated_data)
            parameters = BaseParameter.objects.filter(asset=asset)
            for parameter in parameters:
                parameter.delete()
            #  asset.baseparameter_set.all().delete()  <-- this does not work with the polymorhpic type
            self.parse_parameters(self.parameters_data, asset)
            return asset
