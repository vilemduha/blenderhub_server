
from rest_framework import fields, serializers

from blenderhub.apps.parameters import models as parameter_models


class BaseParameterSerializer(serializers.ModelSerializer):
    parameter_type = serializers.SlugRelatedField(
        slug_field='slug',
        queryset=parameter_models.ParameterType.objects.all(),
    )

    def validate(self, attrs):
        attrs = super().validate(attrs)
        return attrs

    def create(self, validated_data):
        parameter_class = validated_data['parameter_type'].get_parameter_class()
        parameter = parameter_class(**validated_data)
        parameter.clean()
        return parameter

    class Meta:
        model = parameter_models.BaseParameter
        fields = (
            'parameter_type',
            'value',
        )


class StringArrayField(fields.ListField):
    """
    String representation of an array field.
    """
    def to_representation(self, obj):
        obj = super().to_representation(obj)
        # convert list to string
        return ",".join([str(element) for element in obj])

    def to_internal_value(self, data):
        data = data.split(",")  # convert string to list
        return super().to_internal_value(data)


class ArrayParameterSerializer(BaseParameterSerializer):
    value = StringArrayField(read_only=False)


class IntegerParameterSerializer(BaseParameterSerializer):
    value = fields.IntegerField(read_only=False)


class BooleanParameterSerializer(BaseParameterSerializer):
    value = fields.BooleanField(read_only=False)


class FloatParameterSerializer(BaseParameterSerializer):
    value = fields.FloatField(read_only=False)


class PositiveIntegerParameterSerializer(BaseParameterSerializer):
    value = fields.IntegerField(read_only=False, min_value=1)


class CharParameterSerializer(BaseParameterSerializer):
    value = fields.CharField(read_only=False)


class ParameterSerializer(serializers.Serializer):
    class_mapping = {
        'integer': IntegerParameterSerializer,
        'array': ArrayParameterSerializer,
        'positive_integer': PositiveIntegerParameterSerializer,
        'float_type': FloatParameterSerializer,
        'char': CharParameterSerializer,
        'boolean': BooleanParameterSerializer,
    }

    def to_representation(self, obj):
        serializer_class = self.class_mapping[obj.data_type]
        return serializer_class().to_representation(obj)

    def to_internal_value(self, data):
        serializer_class = self.class_mapping[
            parameter_models.ParameterType.objects.get(slug=data['parameter_type']).data_type
        ]
        return serializer_class().to_internal_value(data)

    def create(self, validated_data):
        serializer_class = self.class_mapping[validated_data['parameter_type'].data_type]
        return serializer_class().create(validated_data)
