from rest_framework import exceptions, fields
from rest_framework.serializers import ModelSerializer

from blenderhub.apps.evaluation.models import Rating, Review


class WithUserProfileMixin():
    def create(self, validated_data):
        validated_data['user_profile'] = self.context['request'].user
        return super().create(validated_data)


class RatingSerializer(WithUserProfileMixin, ModelSerializer):
    class Meta:
        model = Rating
        fields = (
            'score',
        )

    def create(self, validated_data):
        if validated_data['rating_type'] not in Rating.UserEditableRatingType.labels.keys():
            raise exceptions.PermissionDenied()
        return super().create(validated_data)

    def update(self, instance, validated_data):
        if 'rating_type' in validated_data or instance.rating_type not in Rating.UserEditableRatingType.labels.keys():
            raise exceptions.PermissionDenied()
        return super().update(instance, validated_data)


class RatingListSerializer(RatingSerializer):
    rating_type = fields.ChoiceField(
        choices=Rating.UserEditableRatingType.choices,
    )

    class Meta(RatingSerializer.Meta):
        fields = RatingSerializer.Meta.fields + (
            'rating_type',
        )


class ReviewSerializer(WithUserProfileMixin, ModelSerializer):
    class Meta:
        model = Review
        fields = (
            'review_text',
            'review_text_problems',
        )

    def create(self, validated_data):
        validated_data.pop('asset__version_uuid')
        return super().create(validated_data)
