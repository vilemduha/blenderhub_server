from rest_framework import serializers
from rest_framework.fields import SerializerMethodField, UUIDField
from rest_framework.reverse import reverse
from rest_framework.serializers import ModelSerializer

from blenderhub.apps.api.exceptions import handle_django_validation_error
from blenderhub.apps.api.serializers import assets as asset_serializer
from blenderhub.apps.assets.models import Asset
from blenderhub.apps.uploads.models import Upload


class UploadSerializer(ModelSerializer):
    id = UUIDField(source='uuid', read_only=True)
    asset = asset_serializer.AssetRefSerializer()
    file_path = serializers.FileField(source='file', required=False)
    # upload_url = URLField(source='get_upload_post_url', read_only=True)
    upload_url = SerializerMethodField(method_name='get_upload_post_url')

    class Meta:
        model = Upload
        fields = (
            'id',
            'asset',
            'file_type',
            'file_path',
            'upload_url',
            # 'metadata',
        )

    def get_upload_post_url(self, obj):
        return reverse('v1:uploads-upload-file', kwargs={'upload_id': obj.uuid}, request=self.context['request'])


# TODO: asset_id vs. nested asset{id}
class UploadCreateSerializer(UploadSerializer):
    asset_id = serializers.UUIDField(source='asset.version_uuid', required=True)

    class Meta:
        model = Upload
        fields = (
            'id',
            'asset_id',
            'file_type',
            'file_path',
            'upload_url',
        )

    def validate(self, attrs):
        attrs['user'] = self.context['request'].user

        try:
            asset_id = attrs.pop('asset')['version_uuid']
            asset = Asset.objects.filter_api_list(author=attrs['user']).get(version_uuid=asset_id)

        except Asset.DoesNotExist:
            raise serializers.ValidationError(
                {'asset_id': "Invalid asset to upload a file for."},
                code='invalid',
            )

        attrs['asset'] = asset

        return attrs

    def create(self, validated_data):
        with handle_django_validation_error():
            return Upload.objects.create_upload(**validated_data)


class UploadUpdateSerializer(UploadSerializer):
    class Meta:
        model = Upload
        fields = (
            'id',
            'file_type',
            'file_path',
            'upload_url',
        )
