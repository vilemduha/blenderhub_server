from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text
from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from blenderhub.apps.assets.models import Asset
from blenderhub.apps.usage_statistics import models


class ProximitySerializer(serializers.ModelSerializer):
    asset_placed_near = serializers.SlugRelatedField(
        slug_field='asset_uuid',
        queryset=Asset.objects.all(),
    )

    class Meta:
        model = models.Proximity
        fields = (
            'distance',
            'asset_placed_near',
        )


class AssetUsageSerializer(WritableNestedModelSerializer):
    asset = serializers.SlugRelatedField(
        slug_field='asset_uuid',
        queryset=Asset.objects.all(),
    )
    proximity_set = ProximitySerializer(many=True)

    class Meta:
        model = models.AssetUsage
        fields = (
            'asset',
            'proximity_set',
            'usage_count',
        )


class CreatableSlugRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        user = self.context['request'].user
        try:
            return self.get_queryset().get_or_create(user_profile=user, **{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class UsageReportSerializer(WritableNestedModelSerializer):
    scene = CreatableSlugRelatedField(
        slug_field='scene_uuid',
        queryset=models.Scene.objects.all(),
    )
    assetusage_set = AssetUsageSerializer(many=True)

    class Meta:
        model = models.UsageReport
        fields = (
            'scene',
            'report_type',
            'created',
            'assetusage_set',
        )
