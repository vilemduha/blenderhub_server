from django.test.utils import override_settings
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase

from blenderhub.apps.accounts.models import UserProfile


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage',
    ES_ENABLED=False,
)
class UploadsTests(APITestCase):
    def setUp(self):
        self.user = UserProfile.objects.create(username='test-user')
        self.client.force_authenticate(user=self.user)

    def test_upload(self):
        """
        Test uploading new file
        """
        asset = mommy.make('Asset', author=self.user)
        url = reverse('v1:uploads-list')
        self.assertEqual(asset.files.count(), 0)
        with open('./blenderhub/apps/api/tests/test_file.txt') as fp:
            post_data = {
                "asset_id": asset.version_uuid,
                "file_type": 'blend',
                "file_path": fp,
            }
            response = self.client.post(url, post_data, format='multipart')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(asset.files.count(), 1)

    # def test_upload_by_requests(self):
    #     """
    #     Test uploading new file
    #     This is here to demonstrate usage by requests library
    #     """
    #     asset = mommy.make('Asset', author=self.user)
    #     url = reverse('v1:uploads-list')
    #     self.assertEqual(asset.files.count(), 0)
    #     with open('./blenderhub/apps/api/tests/test_file.txt') as fp:
    #         upheaders = {
    #             "accept": "application/json",
    #             "Authorization": "Bearer 640b560fda370210c029f12cdec3d2c1f2744583", # % self.user.auth_token,
    #             "Content-Type": "multipart/form-data",
    #         }
    #         print(upheaders)
    #         files = {'filePath': ('report.txt', fp, 'text/plain')}
    #         post_data = {
    #             "assetId": asset.version_uuid,
    #             "fileType": 'blend',
    #         }
    #         url = "http://localhost:8001" + url
    #         print(url)
    #         response = requests.post(url, files=files, data=post_data, headers=upheaders)
    #         print(response)
    #         print(response.content)
    #         self.assertEqual(response.status_code, 201)
    #         self.assertEqual(asset.files.count(), 1)

    def test_upload_nofile(self):
        """
        Test uploading new file
        """
        asset = mommy.make('Asset', author=self.user)
        url = reverse('v1:uploads-list')
        self.assertEqual(asset.files.count(), 0)
        post_data = {
            "asset_id": asset.version_uuid,
            "file_type": 'blend',
        }
        response = self.client.post(url, post_data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(asset.files.count(), 0)
