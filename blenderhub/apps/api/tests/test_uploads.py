from django.test.utils import override_settings
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase

from blenderhub.apps.accounts.models import UserProfile


@override_settings(
    DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage',
    ES_ENABLED=False,
)
class UploadsTests(APITestCase):
    def setUp(self):
        self.user = UserProfile.objects.create(username='test-user')
        self.client.force_authenticate(user=self.user)

    def test_upload(self):
        """
        Test uploading new file
        """
        asset = mommy.make('Asset', author=self.user)
        url = reverse('v1:uploads-list')
        self.assertEqual(asset.files.count(), 0)
        with open('./blenderhub/apps/api/tests/test_file.txt') as fp:
            post_data = {
                "asset_id": asset.version_uuid,
                "file_type": 'blend',
                "file_path": fp,
            }
            response = self.client.post(url, post_data, format='multipart')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(asset.files.count(), 1)

    def test_upload_nofile(self):
        """
        Test uploading new file
        """
        asset = mommy.make('Asset', author=self.user)
        url = reverse('v1:uploads-list')
        self.assertEqual(asset.files.count(), 0)
        post_data = {
            "asset_id": asset.version_uuid,
            "file_type": 'blend',
        }
        response = self.client.post(url, post_data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(asset.files.count(), 0)
