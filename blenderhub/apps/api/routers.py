from rest_framework.routers import DefaultRouter


class ApiRouter(DefaultRouter):
    include_format_suffixes = False
