from rest_framework.authentication import (
    SessionAuthentication, TokenAuthentication
)


class BearerTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'


class SuperAdminSessionAuthentication(SessionAuthentication):
    def authenticate(self, request):
        is_auth = super().authenticate(request)

        if not is_auth:
            return is_auth

        user, token = is_auth

        if not (user.is_staff and user.is_superuser):
            return None

        return user, token
