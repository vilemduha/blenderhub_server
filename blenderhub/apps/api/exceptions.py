from collections import defaultdict
from contextlib import contextmanager

from django.core.exceptions import NON_FIELD_ERRORS
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework import exceptions, serializers
from rest_framework.settings import api_settings
from rest_framework.views import exception_handler as drf_exception_handler


def exception_handler(exc, context):
    if isinstance(exc, exceptions.APIException):
        if isinstance(exc.detail, (list, dict)):
            exc.detail = {'detail': exc.detail}

    response = drf_exception_handler(exc, context)

    if response is not None:
        response.data['status_code'] = response.status_code

    return response


@contextmanager
def handle_django_validation_error():
    try:
        yield

    except DjangoValidationError as e:
        raise convert_django_validation_error_to_drf(e)

    except Exception:
        raise


def convert_django_validation_error_to_drf(src_exc):
    if not isinstance(src_exc, DjangoValidationError):
        return src_exc

    detail = defaultdict(list)

    if hasattr(src_exc, 'error_dict'):
        error_dict = src_exc.error_dict
    elif hasattr(src_exc, 'error_list'):
        error_dict = {NON_FIELD_ERRORS: src_exc.error_list}
    else:
        error_dict = {NON_FIELD_ERRORS: src_exc.messages}

    for key, errs in error_dict.items():
        if key == NON_FIELD_ERRORS:
            key = api_settings.NON_FIELD_ERRORS_KEY

        for e in errs:
            if hasattr(e, 'code'):
                if isinstance(e.messages, list) and len(e.messages) == 1:
                    msg = e.messages[0]
                else:
                    msg = e.messages

                err = {'detail': msg}
                if e.code:
                    err['code'] = e.code

                err = [err]

            else:
                err = e.messages

            detail[key].extend(err)

    return serializers.ValidationError(detail=detail)
