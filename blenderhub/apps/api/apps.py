from django.apps import AppConfig


class ApiAppConfig(AppConfig):
    name = 'blenderhub.apps.api'
