from rest_framework import viewsets

from blenderhub.apps.api.serializers import parameters as parameter_serializers
from blenderhub.apps.parameters.models import BaseParameter


class ParameterSet(viewsets.ModelViewSet):
    # allowed_methods = ('GET',)

    serializer_class = parameter_serializers.ParameterSerializer

    queryset = BaseParameter.objects.all()
