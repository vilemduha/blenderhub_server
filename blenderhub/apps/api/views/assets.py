from rest_framework.decorators import detail_route
from rest_framework.response import Response

from blenderhub.apps.api.serializers import assets as asset_serializers
from blenderhub.apps.api.views.base import CreateUpdateModelViewSet
from blenderhub.apps.assets.models import Asset, AssetFile


class AssetViewSet(CreateUpdateModelViewSet):
    """
    Asset endpoint

    list:
    Returns all assets authored by a current user.

    retrieve:
    Returns a single asset.

    create:
    Creates a new asset or asset version:

    - **new asset (first version)**: *do not* provide `assetBaseId`
    - **new asset version**: provide `assetBaseId` – created asset will be a new version of an existing asset
    referenced by `assetBaseId`

    update:
    Updates a asset attributes and does not create a new asset version.
    All fields will be overwritten. All required fields have to be provided.

    partial_update:
    Updates a asset attributes and does not create a new asset version.
    Only a subset of fields is allowed to be provided.

    """
    serializer_class = asset_serializers.AssetSerializer
    queryset = Asset.objects.all()
    lookup_field = 'version_uuid'
    lookup_url_kwarg = 'asset_id'

    def get_queryset(self):
        return super().get_queryset().filter_api_list(author=self.request.user)

    def get_serializer_class(self):
        if self.request.method in ('POST',):
            return asset_serializers.AssetCreateSerializer
        if self.request.method in ('PUT', 'PATCH',):
            return asset_serializers.AssetUpdateSerializer
        return super().get_serializer_class()

    # noinspection PyUnusedLocal
    @detail_route(methods=['get'])
    def versions(self, request, asset_id=None):
        """
        Retrieves all versions of the given asset.

        """
        asset = self.get_object()
        asset_versions = Asset.objects.filter(asset_uuid=asset.asset_uuid)
        context = {'request': request}
        serializer = asset_serializers.AssetSerializer(data=asset_versions, many=True, context=context)

        serializer.is_valid(raise_exception=False)

        return Response(serializer.data)

    # noinspection PyUnusedLocal
    @detail_route(methods=['get'])
    def files(self, request, asset_id=None):
        """
        Retrieves all files of the given asset.

        """
        asset = self.get_object()
        asset_files = AssetFile.objects.filter(asset=asset)
        serializer = asset_serializers.AssetFileSerializer(data=asset_files, many=True)

        serializer.is_valid(raise_exception=False)

        return Response(serializer.data)
