from rest_framework.response import Response
from rest_framework.views import APIView

from blenderhub.apps.api.serializers.accounts import UserSerializer


class MeView(APIView):
    """
    get:
        The method provides an info about current user.

    """

    # noinspection PyUnusedLocal
    def get(self, request, **kwargs):
        resp = dict(
            me='🐳',
            user=UserSerializer(request.user).data
        )

        return Response(resp)
