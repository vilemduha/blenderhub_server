from django.db import transaction
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView

from blenderhub.apps.api.serializers import uploads as upload_serializers
from blenderhub.apps.api.views.base import CreateUpdateModelViewSet
from blenderhub.apps.uploads.models import Upload


# TODO: uploads

# upload status - ready, finished, timed-out
#   file already uploaded

# upload timeout
# validation:
#   checks
#   content type
#   file types
#   file size
#   anti-virus, anti-malware
#   is file valid for an asset?

# security:
#   https://docs.djangoproject.com/en/2.0/topics/security/#user-uploaded-content
class UploadViewSet(CreateUpdateModelViewSet):
    """
    The endpoint used for uploading asset files

    create:
    Asset file uploading workflow:

    1. create new upload, provide its metadata
    1. in the response object you'll get a URL where you should `PUT` the file data
    1. `PUT` file data to this URL

    """

    serializer_class = upload_serializers.UploadSerializer
    queryset = Upload.objects.all()
    lookup_field = 'uuid'
    lookup_url_kwarg = 'upload_id'

    def get_queryset(self):
        return super().get_queryset().filter_api_list(user=self.request.user)

    def get_serializer_class(self):
        if self.request.method in ('POST'):
            return upload_serializers.UploadCreateSerializer
        if self.request.method in ('PUT'):
            return upload_serializers.UploadUpdateSerializer
        return super().get_serializer_class()


class UploadFileView(APIView):
    """
    put:
    Use the following headers along with the data:

    <code>
    Content-Type: multipart/mixed
    Content-Disposition: form-data; name="file"; filename="&lt;filename&gt;"
    </code>

    """

    parser_classes = (
        FileUploadParser,
    )

    # noinspection PyUnusedLocal
    @transaction.atomic()
    def put(self, request, upload_id, **kwargs):
        if not (request.FILES and request.FILES.get('file')):
            raise ValidationError({'file': 'Missing or invalid file data.'})

        upload_obj = get_object_or_404(Upload.objects.filter_api_list(user=request.user), uuid=upload_id)  # type: Upload

        upload_file_data = request.FILES['file']
        upload_obj.file = upload_file_data
        upload_obj.save()

        return Response({'detail': 'Upload OK.'})
