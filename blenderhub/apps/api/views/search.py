# TODO: query:
#   asset type
# TODO: pagination
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from blenderhub.apps.api.serializers.assets import AssetSerializer
from blenderhub.apps.search import searchers


class SearchView(ListAPIView):
    """
    get:

    The method returns search results.
    Resulting assets are ordered by search score.

    ### Query parameter
    It accepts the following GET parameters:
    - `query`: `?query=search+text` Add any string to search fulltext in `tags`, `name` and `description` fields.
    Or add `key:value` params to search by specified fields. Allowed search keys:
      - `asset_type`: Asset type
      - `license`:  License
      - `adult`: Is adult content?
      - `tags`: Tags

    """

    serializer_class = AssetSerializer

    def get_queryset(self):
        return None

    def list(self, request, *args, **kwargs):
        raw_query = request.GET.get('query', '')
        parsed_query = searchers.parse_query(raw_query)

        searcher = searchers.AssetSearcher()
        queryset = searcher.find_objects(**parsed_query)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
