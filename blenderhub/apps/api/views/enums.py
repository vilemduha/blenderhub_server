
from rest_framework.response import Response
from rest_framework.views import APIView

from blenderhub.apps.assets.models import Asset, AssetFile, AssetType
from blenderhub.apps.parameters.models import ParameterType


class BaseEnumAPIView(APIView):
    ENUM_CHOICES = ()
    FIELD_NAMES = ('id', 'title')

    # noinspection PyUnusedLocal,PyShadowingBuiltins
    def get(self, request, format=None):
        """
        Returns a list of all enum values.
        """
        enums = map(lambda ch: dict(zip(self.FIELD_NAMES, ch)), self.ENUM_CHOICES)
        return Response(enums)


class AssetTypeEnum(BaseEnumAPIView):
    ENUM_CHOICES = AssetType.objects.values_list("name", "value",)


class ParameterTypeEnum(BaseEnumAPIView):
    FIELD_NAMES = (
        "slug",
        "name",
        "auto_generated",
        "data_type",
        "order",
        "allowed_asset_types",
    )
    ENUM_CHOICES = ParameterType.objects.values_list(*FIELD_NAMES)


class AssetConditionEnum(BaseEnumAPIView):
    ENUM_CHOICES = Asset.AssetCondition.choices


class AssetFileTypeEnum(BaseEnumAPIView):
    ENUM_CHOICES = AssetFile.FileType.choices


class ProductionLevelEnum(BaseEnumAPIView):
    ENUM_CHOICES = Asset.ProductionLevel.choices


class LicenseEnum(BaseEnumAPIView):
    ENUM_CHOICES = Asset.License.choices

    # noinspection PyShadowingBuiltins
    def get(self, request, format=None):
        """
        Returns a list of valid licenses.
        """
        return super().get(request, format)

# AssetTypeEnum.get.__doc__ = 'Returns a list of valid asset types.'
# AssetConditionEnum.get.__doc__ = 'Returns a list of valid asset conditions.'
# ProductionLevelEnum.get.__doc__ = 'Returns a list of valid production levels.'
# LicenseEnum.get.__doc__ = 'Returns a list of valid licenses.'
