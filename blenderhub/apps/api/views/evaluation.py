from blenderhub.apps.api.serializers import \
    evaluation as evaluation_serializers
from blenderhub.apps.api.views.base import (
    UpdateDetailModelViewSet, UpdateModelViewSet
)
from blenderhub.apps.assets.models import Asset
from blenderhub.apps.evaluation.models import Rating, Review
from rest_framework_extensions.mixins import NestedViewSetMixin

from .base import AllowPUTAsCreateMixin


class WithUserProfileMixin():
    def get_queryset(self):
        return super().get_queryset().filter(user_profile=self.request.user)


class WithAssetMixin():
    def get_asset_id(self):
        if 'asset__version_uuid' in self.kwargs:
            version_uuid = self.kwargs['asset__version_uuid']
        else:
            version_uuid = self.get_parents_query_dict()['asset__version_uuid']
        return Asset.objects.get(version_uuid=version_uuid)

    def perform_create(self, serializer, **kwargs):
        kwargs['asset'] = self.get_asset_id()
        serializer.save(**kwargs)

    def perform_update(self, serializer):
        serializer.save(asset=self.get_asset_id())


class RatingViewSet(WithAssetMixin, AllowPUTAsCreateMixin, NestedViewSetMixin, WithUserProfileMixin, UpdateModelViewSet):
    lookup_field = 'rating_type'
    serializer_class = evaluation_serializers.RatingListSerializer
    queryset = Rating.objects.filter(rating_type__in=Rating.UserEditableRatingType.labels.keys())

    def get_serializer_class(self):
        if self.request.method in ('PUT', 'PATCH',):
            return evaluation_serializers.RatingSerializer
        return super().get_serializer_class()


class ReviewViewSet(WithAssetMixin, AllowPUTAsCreateMixin, NestedViewSetMixin, WithUserProfileMixin, UpdateDetailModelViewSet):
    lookup_field = 'asset__version_uuid'
    serializer_class = evaluation_serializers.ReviewSerializer
    queryset = Review.objects.all()
