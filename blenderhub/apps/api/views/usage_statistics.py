from blenderhub.apps.api.serializers import \
    usage_statistics as usage_statistics_serializers
from blenderhub.apps.api.views.base import CreateUpdateModelViewSet
from blenderhub.apps.usage_statistics.models import UsageReport


class UsageReportViewSet(CreateUpdateModelViewSet):
    serializer_class = usage_statistics_serializers.UsageReportSerializer
    queryset = UsageReport.objects.all()
