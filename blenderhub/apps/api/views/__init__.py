from django.conf import settings
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

DESCRIPTION = """
<p>Welcome to BlenderKit API service.</p>

<h2>Authentication</h2>
<p>
    A HTTP <code>Authorization</code> header with the API token has to be provided with each request:
    <code>Authorization: Bearer &lt;api_token&gt;</code>
</p>
<p>
    Example:
    <code>curl -X GET "https://{host}/api/v1/me/" -H "accept: application/json" -H "Authorization: Bearer &lt;api_token&gt;"</code>
</p>

""".format(host=settings.DEFAULT_HOST)

SCHEMA_VIEW = get_schema_view(
    openapi.Info(
        default_version='v1',
        _version='v1',
        # url='/api/v1/',

        title="%s API" % settings.PROJECT_NAME,
        description=DESCRIPTION,
        # terms_of_service="http://%s/terms/" % settings.DEFAULT_HOST,
        # contact=openapi.Contact(email="zdenek@softic.cz"),
        # license=openapi.License(name="BSD License"),
    ),
    # validators=['flex', 'ssv'],
    public=True,
    permission_classes=(permissions.AllowAny,),
)
