from registration.forms import RegistrationForm

from .models import UserProfile


class FullUserRegistrationForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = UserProfile
        fields = [
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
