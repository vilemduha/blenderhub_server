from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models


class UserProfileAdmin(UserAdmin):
    list_display = (
        'email',
        'first_name',
        'last_name',
        'is_active',
        'is_staff',
        'is_superuser',
        'last_login',
        'date_joined',
    )


admin.site.register(models.UserProfile, UserProfileAdmin)
