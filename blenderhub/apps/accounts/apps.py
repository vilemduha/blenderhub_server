from django.apps import AppConfig


class AccountsAppConfig(AppConfig):
    name = 'blenderhub.apps.accounts'

    def ready(self):
        from . import signals  # noqa
