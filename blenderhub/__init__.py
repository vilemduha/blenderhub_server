def get_version():
    return 1, 0, 0, 'dev1'


__version__ = get_version()
__versionstr__ = '.'.join(map(str, __version__))
