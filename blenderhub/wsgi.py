from django.core.wsgi import get_wsgi_application
from dotenv import find_dotenv, load_dotenv
from raven.contrib.django.raven_compat.middleware.wsgi import Sentry


load_dotenv(find_dotenv())

application = Sentry(get_wsgi_application())
