from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin, auth
from django.urls import include, path
from registration.backends.default.views import RegistrationView

from blenderhub.apps.accounts.forms import FullUserRegistrationForm


urlpatterns = [
    path('', include('blenderhub.apps.frontend.urls')),
    path('admin/', admin.site.urls),
    path(
        'accounts/register/',
        RegistrationView.as_view(form_class=FullUserRegistrationForm),
        name='registration_register',
    ),
    path('accounts/', include('registration.backends.default.urls')),
    path('reset/<uidb64>/<token>/', auth.views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^nested_admin/', include('nested_admin.urls')),

    url('^api/v1/', include('blenderhub.apps.api.urls_v1', namespace='v1'), name='api_v1'),
    # url('^api/v2/', include('blenderhub.apps.api.urls_v1', namespace='v2'), name='api_v2'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
