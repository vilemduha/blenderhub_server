#!/usr/bin/env python
import os

from setuptools import find_packages, setup

import blenderhub


BASE = os.path.dirname(os.path.abspath(__file__))

setup_requires = ('pytest-runner', 'setuptools',)

setup(
    name='blenderhub',
    version=blenderhub.__versionstr__,
    author='Zdenek Softic',
    author_email='zdenek@softic.cz',
    url='',
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    scripts=[
        'manage.py',
    ],
    setup_requires=setup_requires,
    zip_safe=False,
)
